﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HEFBoilerMS_M1
{
    public class Thermometer
    {
        Texture back;
        Sprite backSprite;

        Texture arrow;
        Sprite arrowSprite;

        RenderWindow window;
        Font font;

        RectangleShape bgRect;

        Text captionText;
        Text temperatureText;

        Text text0;
        Text text20;
        Text text40;
        Text text60;
        Text text80;
        Text text100;
        Text text120;
        Text text140;
        Text text160;
        Text text180;

        String caption;

        public Thermometer(RenderWindow awindow, Font afont, String acaption)
        {
            window = awindow;
            font = afont;
            caption = acaption;

            back = new Texture(".\\media\\thermometer.base.png");
            backSprite = new Sprite(back);

            arrow = new Texture(".\\media\\thermometer_arrow.png");
            arrowSprite = new Sprite(arrow);

            backSprite.Position = new Vector2f(100f, 100f);
            arrowSprite.Position = new Vector2f(100f, 200f);

            captionText = new Text(caption, font);
            captionText.CharacterSize = 12;
            captionText.Color = Color.Black;
            captionText.Position = new Vector2f(20, 230);

            temperatureText = new Text("0.00", font);
            temperatureText.CharacterSize = 20;
            temperatureText.Color = Color.White;
            temperatureText.Position = new Vector2f(100, 150);

            bgRect = new RectangleShape(new Vector2f(120.0f, 270.0f));
            bgRect.FillColor = Color.White;
            bgRect.Position = new Vector2f(139, 139);
            bgRect.Origin = new Vector2f(0, 0);

            text0 = makeText("0", new Vector2f(17, 211));
            text20 = makeText("20", new Vector2f(47, 211));
            text40 = makeText("40", new Vector2f(47, 211));
            text60 = makeText("60", new Vector2f(47, 211));
            text80 = makeText("80", new Vector2f(47, 211));
            text100 = makeText("100", new Vector2f(47, 211));
            text120 = makeText("120", new Vector2f(47, 211));
            text140 = makeText("140", new Vector2f(47, 211));
            text160 = makeText("160", new Vector2f(47, 211));
            text180 = makeText("180", new Vector2f(47, 211));
        }

        Text makeText(String caption, Vector2f pos)
        {
            var text = new Text(caption, font);
            text.CharacterSize = 12;
            text.Color = Color.Black;
            text.Position = pos;
            return text;
        }

        public void Draw()
        {
            window.Draw(bgRect);
            window.Draw(backSprite);
            window.Draw(arrowSprite);
            window.Draw(captionText);
            window.Draw(temperatureText);

            window.Draw(text0);
            window.Draw(text20);
            window.Draw(text40);
            window.Draw(text60);
            window.Draw(text80);
            window.Draw(text100);
            window.Draw(text120);
            window.Draw(text140);
            window.Draw(text160);
            window.Draw(text180);
        }

        internal void Update()
        {
        }

        private float temperature;

        const float MIN_TEMPERATURE = 0;
        const float MAX_TEMPERATURE = 180;
        const float DELTA_TEMPERATURE = MAX_TEMPERATURE - MIN_TEMPERATURE;

        const float RULER_START = 100;
        const float RULER_END = 300;
        const float RULER_HEIGHT = RULER_END - RULER_START;

        public float Temperature 
        {
            get
            {
                return temperature;
            }
            set
            {
                temperature = value;
                UpdateValueText();
                UpdateArrowPosition();
                UpdateTemperaturePosition();
            }
        }

        private void UpdateArrowPosition()
        {
            float dy = (temperature - MIN_TEMPERATURE) / (DELTA_TEMPERATURE) * RULER_HEIGHT;
            arrowSprite.Position = new Vector2f(position.X + 13, position.Y - dy + RULER_START + 90);
        }

        private void UpdateValueText()
        {
            temperatureText.DisplayedString = temperature.ToString("##"); 
        }

        private Vector2f position;
        public Vector2f Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                UpdatePosition();
            }
        }

        private void UpdatePosition()
        {
            backSprite.Position = position;
            captionText.Position = new Vector2f(position.X + 5, position.Y + 240);

            UpdateTemperaturePosition();
            bgRect.Position = position;
            UpdateArrowPosition();
            UpdateTextPosition();
        }

        private void UpdateTextPosition()
        {
            float dx = 50, dy = 08;
            text0.Position = new Vector2f(position.X + dx, position.Y + 200 - dy);
            text20.Position = new Vector2f(position.X + dx, position.Y + 180 - dy);
            text40.Position = new Vector2f(position.X + dx, position.Y + 160 - dy);
            text60.Position = new Vector2f(position.X + dx, position.Y + 140 - dy);
            text80.Position = new Vector2f(position.X + dx, position.Y + 120 - dy);
            text100.Position = new Vector2f(position.X + dx, position.Y + 100 - dy);
            text120.Position = new Vector2f(position.X + dx, position.Y +  80 - dy);
            text140.Position = new Vector2f(position.X + dx, position.Y +  60 - dy);
            text160.Position = new Vector2f(position.X + dx, position.Y +  40 - dy);
            text180.Position = new Vector2f(position.X + dx, position.Y +  20 - dy);
        }

        private void UpdateTemperaturePosition()
        {
            FloatRect bounds = temperatureText.GetLocalBounds();
            temperatureText.Position = new Vector2f(position.X + 60 - bounds.Width/2, position.Y + 205);
        }


    }
}
