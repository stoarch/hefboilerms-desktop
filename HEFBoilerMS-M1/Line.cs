﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HEFBoilerMS_M1
{
    public class Line : Transformable, Drawable
    {
        public Line()
        {
            previous = new Vector2f(-1, -1);
            vertices = new VertexArray();
            vertices.PrimitiveType = PrimitiveType.TrianglesStrip;
        }

        public void Add(Vector2f point, uint thickness, Color acolor )
        {
            if(previous.X != -1 && previous.Y != -1)
            {
                var p1 = previous;
                var p2 = point;
                var v = p2 - p1;

                float len = (float)Length(v); 
                v.X /= len;
                v.Y /= len;
                
                Vector2f vp= new Vector2f(-v.Y, v.X);

                Vector2f temp;
                temp.X = p1.X + pwidth / 2 * vp.X;
                temp.Y = p1.Y + pwidth / 2 * vp.Y;
                vertices.Append(new Vertex(temp, color));

                temp.X = p1.X - pwidth / 2 * vp.X;
                temp.Y = p1.Y - pwidth / 2 * vp.Y;
                vertices.Append(new Vertex(temp, color));

                temp.X = p2.X + thickness / 2 * vp.X;
                temp.Y = p2.Y + thickness / 2 * vp.Y;
                vertices.Append(new Vertex(temp, color));

                temp.X = p2.X - thickness / 2 * vp.X;
                temp.Y = p2.Y - thickness / 2 * vp.Y;
                vertices.Append(new Vertex(temp, color));                    
            }
            previous = point;
            pwidth = thickness;
            color = acolor;
        }

        public void Clear()
        {
            vertices.Clear();
            previous = new Vector2f(-1, -1);
        }

        double Length(Vector2f v)
        {
            return Math.Sqrt(v.X * v.X + v.Y * v.Y);
        }

        public uint size()
        {
            return vertices.VertexCount;
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            states.Transform *= Transform;
            target.Draw(vertices, states);
        }

        private VertexArray vertices;
        private Vector2f previous;
        private uint pwidth;
        private Color color;

    }
}
