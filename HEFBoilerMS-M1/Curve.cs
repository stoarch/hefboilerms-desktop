﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HEFBoilerMS_M1
{
    public class Curve : Transformable, Drawable
    {
        public Curve()
        {
            color = Color.White;
            size = new Vector2f(100, 100);
            fill = true;
            thickness = 3;
            limit = 100;
            area = new Area();
            line = new Line();

            data = new List<float>();
        }

        public Curve(Vector2f asize, Color acolor)
        {
            color = acolor;
            size = asize;
            fill = true;
            thickness = 3;
            limit = 100;
            area = new Area();
            line = new Line();
            data = new List<float>();
        }

        public void SetLabel(String alabel)
        {
            label = alabel;
        }

        public void SetColor(Color acolor)
        {
            color = acolor;
            fillColor = acolor;
            fillColor.A = 100;
        }

        public void AddValue(float value)
        {
            data.Add(value);
            if (data.Count > limit)
                data.RemoveAt(0);
        }

        public void Prepare(ref Vector2f rangex, ref Vector2f rangey)
        {
            float minX = 0;
            float maxX = data.Count;
            float minY = rangey.X;
            float maxY = rangey.Y;

            if( data.Count > 1 )
            {
                if( minY == float.MaxValue )
                {
                    minY = data.First();
                }
                if( maxY == float.MinValue )
                {
                    maxY = data.First();
                }

                for(int i = 0; i < data.Count; i++ )
                {
                    var item = data[i];

                    if (item < minY)
                        minY = item;
                    if (item > maxY)
                        maxY = item;
                }
            }

            rangey.X = minY;
            rangey.Y = maxY;

            rangex.X = minX;
            rangex.Y = maxX;

            line.Clear();
            area.Clear();
            float distance = Math.Abs(maxY - minY);
            if (distance < float.Epsilon)
                distance = 1;

            int xoffset = Convert.ToInt32(size.X / data.Count);

            if (xoffset * data.Count < size.X + xoffset)
                if (data.Count > 1)
                    xoffset = (int)(size.X / (data.Count - 1));
                else
                    xoffset = 0;

            int k = 0;
            float x = 0;
            for (int j = data.Count - 1; j > 0 ; j--)
            {
                var item = data[j];
                
                x = size.X - (k++ * xoffset);
                float realy = (item - minY) / distance;
                float y = size.Y - size.Y * realy;

                x = (x < 0) ? 0 : x;

                line.Add(new Vector2f(x, y), (uint)thickness, color);
                area.Add(new Vector2f(x, y), fillColor, size.Y );
            }
        }

        public void SetFill(bool afill)
        {
            fill = afill;
        }

        public void SetThickness(float athickness)
        {
            thickness = athickness;
        }

        public void SetLimit(uint alimit)
        {
            limit = alimit;
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            states.Transform *= Transform;
            if (fill)
                target.Draw(area, states);
            target.Draw(line, states);
        }

        private String label;
        private Color color;
        private Color fillColor;
        private List<float> data;
        private Vector2f size;

        private bool fill;
        private Line line;
        private Area area;
        private float thickness;
        private uint limit;
    }
}
