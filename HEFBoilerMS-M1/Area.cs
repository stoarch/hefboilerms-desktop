﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HEFBoilerMS_M1
{
    public class Area : Transformable, Drawable
    {
        public Area()
        {
            vertices = new VertexArray();
            vertices.PrimitiveType = PrimitiveType.TrianglesStrip;
        }

        public void Add(Vector2f point, Color color, float sizey = 100)
        {
            vertices.Append(new Vertex(point, color));
            vertices.Append(new Vertex(new Vector2f(point.X, point.Y), color));
        }

        public void Clear()
        {
            vertices.Clear();
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            states.Transform *= Transform;
            target.Draw(vertices, states);
        }

        private VertexArray vertices;
    }
}
