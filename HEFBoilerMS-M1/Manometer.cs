﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HEFBoilerMS_M1
{
    class Manometer
    {
        Texture manometerBack;
        Sprite manometerBackSprite;

        Texture manometerArrow;
        Sprite manometerArrowSprite;

        RenderWindow window;
        Font font;

        RectangleShape bgRect;

        Text captionText;
        Text pressureText;

        Text text0;
        Text text1;
        Text text2;
        Text text3;
        Text text4;
        Text text5;
        Text text6;
        Text text7;
        Text text8;
        Text text9;

        String caption;

        public Manometer(RenderWindow awindow, Font afont, String acaption)
        {
            manometerBack = new Texture(".\\media\\manometer.png");
            manometerBackSprite = new Sprite(manometerBack);

            manometerArrow = new Texture(".\\media\\manometer-arrow.png");
            manometerArrowSprite = new Sprite(manometerArrow);

            manometerArrowSprite.Origin = new Vector2f(100, 64);
            manometerArrowSprite.Position = new Vector2f(120, 120);
            manometerArrowSprite.Rotation = 45;

            window = awindow;

            font = afont;

            caption = acaption;
            captionText = new Text(caption, font);
            captionText.CharacterSize = 12;
            captionText.Color = Color.Black;
            captionText.Position = new Vector2f(20, 230);

            pressureText = new Text("0.00", font);
            pressureText.CharacterSize = 20;
            pressureText.Color = Color.Black;
            pressureText.Position = new Vector2f(100, 150);

            bgRect = new RectangleShape(new Vector2f(238.0f, 260.0f));
            bgRect.FillColor = Color.White;
            bgRect.Position = new Vector2f(139, 139);
            bgRect.Origin = new Vector2f(0, 0);

            text0 = makeText("0", new Vector2f(47, 211));
            text1 = makeText("1", new Vector2f(17, 154));
            text2 = makeText("2", new Vector2f(18, 89));
            text3 = makeText("3", new Vector2f(43, 33));
            text4 = makeText("4", new Vector2f(96, 13));
            text5 = makeText("5", new Vector2f(160, 16));
            text6 = makeText("6", new Vector2f(207, 40));
            text7 = makeText("7", new Vector2f(232, 92));
            text8 = makeText("8", new Vector2f(233, 151));
            text9 = makeText("9", new Vector2f(205, 203));
        }

        Text makeText(String caption, Vector2f pos)
        {
            var text = new Text(caption, font);
            text.CharacterSize = 18;
            text.Color = Color.Black;
            text.Position = pos;
            return text;
        }

        public void Draw()
        {
            window.Draw(bgRect);
            window.Draw(manometerBackSprite);
            window.Draw(manometerArrowSprite);
            window.Draw(captionText);
            window.Draw(pressureText);

            window.Draw(text0);
            window.Draw(text1);
            window.Draw(text2);
            window.Draw(text3);
            window.Draw(text4);
            window.Draw(text5);
            window.Draw(text6);
            window.Draw(text7);
            window.Draw(text8);
            window.Draw(text9);
        }

        internal void Update()
        {
        }

        private float pressure;

        const float MAX_PRESSURE = 9;
        const float MAX_ANGLE = 270;

        public float Pressure
        {
            get
            {
                return pressure;
            }
            set
            {
                pressure = value;
                if (pressure > MAX_PRESSURE)
                    pressure = MAX_PRESSURE;
                UpdatePressureRotation();
                UpdateValueText();
            }
        }

        private void UpdateValueText()
        {
            pressureText.DisplayedString = pressure.ToString("###.00"); 
        }

        private void UpdatePressureRotation()
        {
            float minAngle = 0, maxAngle = 0;
            float dp = 0;
            if(pressure < 1)
            {
                dp = 1 - pressure;
                minAngle = -44.46f;
                maxAngle = -12.00f;
            }
            else if(pressure < 2 )
            {
                dp = 2 - pressure;
                minAngle = -13.68f;
                maxAngle = 18.50f;
            }
            else if(pressure < 3)
            {
                dp = 3 - pressure;
                minAngle = 16.83f;
                maxAngle = 51.37f;
            }
            else if( pressure < 4)
            {
                dp = 4 - pressure;
                minAngle = 44.37f;
                maxAngle = 76.34f;
            }
            else if(pressure < 5)
            {
                dp = 5 - pressure;
                minAngle = 74.34f;
                maxAngle = 108.04f;
            }
            else if(pressure < 6)
            {
                dp = 6 - pressure;
                minAngle = 104.04f;
                maxAngle = 137.28f;
            }
            else if(pressure < 7)
            {
                dp = 7 - pressure;
                minAngle = 134.28f;
                maxAngle = 165.87f;
            }
            else if(pressure < 8)
            {
                dp = 8 - pressure;
                minAngle = 165.87f;
                maxAngle = 199.76f;
            }
            else if(pressure < 9)
            {
                if (pressure > 9)
                    dp = 1;
                else
                    dp = 9 - pressure;

                minAngle = 194.76f;
                maxAngle = 228f;
            }

            float dangle = (1 - dp)/(maxAngle - minAngle)*1000;
            manometerArrowSprite.Rotation = minAngle + dangle;
        }

        private Vector2f position;
        public Vector2f Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                UpdatePosition();
            }
        }

        private void UpdatePosition()
        {
            manometerBackSprite.Position = position;
            manometerArrowSprite.Position = new Vector2f(position.X + 120, position.Y + 120);
            captionText.Position = new Vector2f(position.X + 20, position.Y + 230);
            pressureText.Position = new Vector2f(position.X + 100, position.Y + 150);
            bgRect.Position = position;

            text0 = makeText("0", new Vector2f(position.X + 37, position.Y + 191));
            text1 = makeText("1", new Vector2f(position.X + 07, position.Y + 134));
            text2 = makeText("2", new Vector2f(position.X + 08, position.Y + 74));
            text3 = makeText("3", new Vector2f(position.X + 36, position.Y + 26));
            text4 = makeText("4", new Vector2f(position.X + 86, position.Y + 00));
            text5 = makeText("5", new Vector2f(position.X + 145, position.Y + 00));
            text6 = makeText("6", new Vector2f(position.X + 193, position.Y + 32));
            text7 = makeText("7", new Vector2f(position.X + 222, position.Y + 82));
            text8 = makeText("8", new Vector2f(position.X + 220, position.Y + 136));
            text9 = makeText("9", new Vector2f(position.X + 193, position.Y + 190));
        }

    }
}
