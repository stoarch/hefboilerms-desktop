﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HEFBoilerMS_M1
{
    public class Axis : Transformable, Drawable
    {
        private const int kPart = 2;

        public Axis()
        {
            label = new Text();
            name = "";
            numbers = new List<Text>();
        }

        public void SetSize(float asize)
        {
            size = asize; 
        }

        public void SetName(String aname)
        {
            name = aname;
            DefineLabel();
        }
        
        public void Prepare(Vector2f range)
        {
            if (font == null)
                return;

            numbers.Clear();

            float distance = Math.Abs(range.Y - range.X);

            if (distance == 0)
                distance = 1;

            float decx = size / kPart;
            float offset = distance / kPart;

            for (int i = 0; i <= kPart; i++)
            {
                Text text = new Text();
                text.Font = font;
                text.CharacterSize = 12;
                text.Color = Color.Blue;
                String val = (range.X + offset*i).ToString("0.0");
                text.DisplayedString = val;
                text.Position = new Vector2f(i * decx - val.Length * 2, 0);
                numbers.Add(text);
            }
        }

        public void SetFont(Font afont)
        {
            font = afont;
            DefineLabel();
        }

        private void DefineLabel()
        {
            if (font == null)
                return;

            float decx = size / kPart;

            label.Font = font;
            label.CharacterSize = 14;
            label.Color = Color.Black;
            label.DisplayedString = name;

            if (Rotation != 0)
                label.Position = new Vector2f( Position.X + size/kPart - name.Length*5, -20);
            else
                label.Position = new Vector2f( Position.X + size/kPart - name.Length*5, 20);
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            if (font == null)
                return;

            states.Transform *= Transform;
            foreach(var item in numbers)
            {
                target.Draw(item, states);
            }
            target.Draw(label);
        }
        
        private String name;
        private Font font;
        private float size;
        private Text label;
        private List<Text> numbers;
    }
}
