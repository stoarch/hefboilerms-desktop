﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace HEFBoilerMS_M1
{
    public class FlowLineChart : Transformable, Drawable
    {
        private Plot plot;
        private String dns;
        private Random rnd;

        public FlowLineChart(String caption, Vector2i location, String xcaption, String ycaption)
        {
            rnd = new Random();
            plot = new Plot(new Vector2f(330,130),"chart");
            plot.SetTitle(caption);
            plot.setSize(new Vector2f(330, 130));
            plot.setFont(".\\media\\fonts\\consola.ttf");
            plot.SetXLabel(xcaption);
            plot.SetYLabel(ycaption);
            plot.setBackgroundColor(new Color(120, 110, 200));
            plot.setTitleColor(Color.Yellow);
            plot.setPosition( new Vector2f(location.X, location.Y));
            Curve curve = plot.createCurve("chart", Color.Black);
            curve.SetFill(true);
            curve.SetThickness(2);
            curve.SetLimit((uint)(100));
        }

        private int val = 0;
        public void Update(float value)
        {
            Curve curve = plot.getCurve("chart");
            curve.AddValue(value);
            plot.Prepare();
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            target.Draw(plot, states);
        }

        float ping(String dns)
        {
            Ping sender = new Ping();
            PingReply reply = sender.Send(dns);
            if(reply.Status == IPStatus.Success)
            {
                return (float)reply.RoundtripTime;
            }
            return 0.0f;
        }
    }
}
