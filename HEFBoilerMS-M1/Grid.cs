﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HEFBoilerMS_M1
{
    public class Grid : Transformable, Drawable
    {
        const int kPart = 4;

        public Grid()
        {
            border = new RectangleShape(); 
            vertices = new VertexArray();
        }

        public void Setup(Vector2f asize)
        {
            color = new Color(150, 150, 150);
            size = asize;
            vertices.Clear();
            vertices.PrimitiveType = PrimitiveType.Lines;
            float offset = size.Y / kPart;
            for (int i = 0; i < kPart; i++)
            {
                vertices.Append(new Vertex(new Vector2f(0, i * offset), color));
                vertices.Append(new Vertex(new Vector2f(size.X, i * offset), color));
            }

            offset = size.X / kPart;
            for (int i = 0; i < kPart; i++)
            {
                vertices.Append(new Vertex(new Vector2f(i * offset, 0), color));
                vertices.Append(new Vertex(new Vector2f(i * offset, size.Y), color));
            }

            border.Size = size;
            border.FillColor = new Color(0, 0, 0);
            border.OutlineColor = new Color(0, 0, 0);
            border.OutlineThickness = 1;
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            states.Transform *= Transform;
        }

        private VertexArray vertices;
        private RectangleShape border;
        private Color color;
        private Vector2f size;
    }
}
