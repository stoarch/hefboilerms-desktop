﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using SFML.Graphics;
using SFML.Window;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using HEFBoilerMS_M1.Properties;

namespace HEFBoilerMS_M1
{
    static class Program
    {
        static float mouseX, mouseY;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            RenderWindow window = new RenderWindow(new VideoMode(1360, 768), "HEF Boiler MS - M1");
            window.Closed += new EventHandler(OnClose);
            window.KeyPressed += window_KeyPressed;

            window.MouseMoved += window_MouseMoved;

            var windowColor = new Color(0, 92, 55);


            float frametime = 1.0f / 60.0f;
            Font font = new Font(".\\media\\fonts\\consola.ttf");

            var textX = new Text("0", font);
            textX.CharacterSize = 12;
            textX.Color = Color.Black;
            textX.Position = new Vector2f( 20, 10 );

            var textY = new Text("0", font);
            textY.CharacterSize = 12;
            textY.Color = Color.Black;
            textY.Position = new Vector2f( 50, 10 );
            
            window.SetFramerateLimit(60);

            var supHeatSteamAfterBoilerManometer = new Manometer(window, font, "Давление перегретого пара\nперед котлом");
            var feedwaterOnBoilerManometer = new Manometer(window, font, "Давление питательной воды\nв котле");

            var steamAfterBoilerThermometer = new Thermometer(window, font, "Температура пара\nпосле котла");
            var steam2AfterBoilerThermometer = new Thermometer(window, font, "Температура 2\nпара после котла");
            var feedwaterAndExhGasesThermometer = new Thermometer(window, font, "Температура пит.\nводы после котла");

            var airBeforeBurnersFlowmeter = new Flowmeter(window, font, "Расход воздуха до горелок");
            var gasOnBoilerFlowmeter = new Flowmeter(window, font, "Расход газа на котле");
            var supheatSteamAfterBoilerFlowmeter = new Flowmeter(window, font, "Расход перегретого пара после котла");
            var feedwaterWaterOnBoilerFlowmeter = new Flowmeter(window, font, "Расход питательной воды в котле");

            Flowmeter waterLevelOnDrum = new Flowmeter(window, font, "Уровень воды в барабане");

            supHeatSteamAfterBoilerManometer.Position = new Vector2f(10, 10);
            feedwaterOnBoilerManometer.Position = new Vector2f(250, 10);

            waterLevelOnDrum.Position = new Vector2f(930, 10);

            steamAfterBoilerThermometer.Position = new Vector2f(500, 10);
            steam2AfterBoilerThermometer.Position = new Vector2f(630, 10);
            feedwaterAndExhGasesThermometer.Position = new Vector2f(760, 10);

            airBeforeBurnersFlowmeter.Position = new Vector2f(1070, 110);
            gasOnBoilerFlowmeter.Position = new Vector2f(1070, 200);
            supheatSteamAfterBoilerFlowmeter.Position = new Vector2f(1070, 290);
            feedwaterWaterOnBoilerFlowmeter.Position = new Vector2f(1070, 380);

            var factory = new ConnectionFactory();
            factory.UserName = "ms";
            factory.Password = "msqA115";
            factory.VirtualHost = "/";
            factory.Protocol = Protocols.DefaultProtocol;
            factory.HostName = Properties.Settings.Default.ServerURL; 
            factory.Port = AmqpTcpEndpoint.UseDefaultPort;

            float gasBeforeBoilerFlowrate = 0.0f;
            float drumWaterLevel = 0.0f;            
            float supHeatSteamAfterBoilerPressure = 0.0f;
            float steamAfterBoilerTemperature = 0.0f;
            float steam2AfterBoilerTemperature = 0.0f;
            float supheatSteamAfterBoilerFlowrate = 0.0f;
            float feedwaterOnBoilerFlowrate = 0.0f;
            float feedwaterOnBoilerPressure = 0.0f;
            float feedwaterAndExhGasesTemperature = 0.0f;
            float airBeforeBurnersFlowrate = 0.0f;

            var supHeatSteamFlowLineChart = new FlowLineChart("Расход перегретого пара после котла", new Vector2i(15, 310), "сек", "т/ч" );
            var steamAfterBoilerTempChart = new FlowLineChart("Температура пара после котла", new Vector2i(15, 470), "сек", "т/ч" );
            var steam2AfterBoilerTempChart = new FlowLineChart("Температура (2) пара после котла", new Vector2i(15, 630), "сек", "т/ч" );

            var feedwaterOnBoilerPressChart = new FlowLineChart("Давление питательной воды в котле", new Vector2i(353, 310), "сек", "т/ч" );
            var feedwaterOnBoilerFlowChart = new FlowLineChart("Расход питательной воды в котле", new Vector2i(353, 470), "сек", "т/ч" );
            var feedwaterOnBoilerTempChart = new FlowLineChart("Температура питат. воды в котле", new Vector2i(353, 630), "сек", "т/ч" );

            var airBeforeBurnersFlowChart = new FlowLineChart("Расход вохдуха до горелок", new Vector2i(690, 310), "сек", "т/ч" );
            var gasBeforeBoilerFlowChart = new FlowLineChart("Расход газа на котле", new Vector2i(690, 470), "сек", "т/ч" );
            var supheatSteamFlowChart = new FlowLineChart("Расход перегретого пара после котла", new Vector2i(690, 630), "сек", "т/ч" );

            var waterLevelOnDrumChart  = new FlowLineChart("Уровень воды в барабане", new Vector2i(1025, 630), "сек", "т/ч" );

            Time time = new Time();
            double prevTime = 0; 

            using( var connection = factory.CreateConnection())
            using( var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: "vals", type: "fanout");
                var queueName = channel.QueueDeclare().QueueName;
                channel.QueueBind(queue: queueName, 
                    exchange: "vals",
                    routingKey: "");

                char[] delims = { ' ' };

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body;
                        var message = Encoding.UTF8.GetString(body);
                        string[] vals = message.Split(delims);
                        vals[2] = vals[2].Replace('.', ',');

                        try
                        {
                            switch (vals[0])
                            {
                                case "1": gasBeforeBoilerFlowrate = float.Parse(vals[2]); break;
                                case "2": drumWaterLevel = float.Parse(vals[2]); break;
                                case "3": supHeatSteamAfterBoilerPressure = float.Parse(vals[2]); break;
                                case "4": steamAfterBoilerTemperature = float.Parse(vals[2]); break;
                                case "5": steam2AfterBoilerTemperature = float.Parse(vals[2]); break;
                                case "6": supheatSteamAfterBoilerFlowrate = float.Parse(vals[2]); break;
                                case "7": feedwaterOnBoilerFlowrate = float.Parse(vals[2]); break;
                                case "8": feedwaterAndExhGasesTemperature = float.Parse(vals[2]); break;
                                case "9": feedwaterOnBoilerPressure = float.Parse(vals[2]); break;
                                case "10": airBeforeBurnersFlowrate = float.Parse(vals[2]); break;
                            };
                        }
                        catch (Exception e)
                        {
                            Debug.WriteLine("Unable to parse " + message);
                        }

                    };

                channel.BasicConsume(queue: queueName, noAck: true, consumer: consumer);

                while (window.IsOpen())
                {
                    time.Update();
                    window.DispatchEvents();
                    window.Clear(windowColor);

                    if( time.TotalElapsedTime - prevTime > 1.0 )
                    {
                        prevTime = time.TotalElapsedTime;

                        supHeatSteamFlowLineChart.Update(supheatSteamAfterBoilerFlowrate);
                        steamAfterBoilerTempChart.Update(steamAfterBoilerTemperature);
                        steam2AfterBoilerTempChart.Update(steam2AfterBoilerTemperature);

                        feedwaterOnBoilerPressChart.Update(feedwaterOnBoilerPressure);
                        feedwaterOnBoilerFlowChart.Update(feedwaterOnBoilerFlowrate);
                        feedwaterOnBoilerTempChart.Update(feedwaterAndExhGasesTemperature);

                        airBeforeBurnersFlowChart.Update(airBeforeBurnersFlowrate);
                        gasBeforeBoilerFlowChart.Update(gasBeforeBoilerFlowrate);
                        supheatSteamFlowChart.Update(supheatSteamAfterBoilerFlowrate);

                        waterLevelOnDrumChart.Update(drumWaterLevel);
                    }

                    window.Draw(supHeatSteamFlowLineChart);
                    window.Draw(steamAfterBoilerTempChart);
                    window.Draw(steam2AfterBoilerTempChart);

                    window.Draw(feedwaterOnBoilerPressChart);
                    window.Draw(feedwaterOnBoilerFlowChart);
                    window.Draw(feedwaterOnBoilerTempChart);

                    window.Draw(airBeforeBurnersFlowChart);
                    window.Draw(gasBeforeBoilerFlowChart);
                    window.Draw(supheatSteamFlowChart);

                    window.Draw(waterLevelOnDrumChart);


                    supHeatSteamAfterBoilerManometer.Draw();
                    feedwaterOnBoilerManometer.Draw();
                    steamAfterBoilerThermometer.Draw();
                    steam2AfterBoilerThermometer.Draw();
                    feedwaterAndExhGasesThermometer.Draw();
                    airBeforeBurnersFlowmeter.Draw();
                    gasOnBoilerFlowmeter.Draw();
                    supheatSteamAfterBoilerFlowmeter.Draw();
                    feedwaterWaterOnBoilerFlowmeter.Draw();
                    waterLevelOnDrum.Draw();

                    textX.DisplayedString = mouseX.ToString();
                    textY.DisplayedString = mouseY.ToString();
                    window.Draw(textX);
                    window.Draw(textY);

                    
                    window.Display();

                    supHeatSteamAfterBoilerManometer.Pressure = supHeatSteamAfterBoilerPressure;
                    feedwaterOnBoilerManometer.Pressure = feedwaterOnBoilerPressure;

                    steamAfterBoilerThermometer.Temperature = steamAfterBoilerTemperature;
                    steam2AfterBoilerThermometer.Temperature = steam2AfterBoilerTemperature;
                    feedwaterAndExhGasesThermometer.Temperature = feedwaterAndExhGasesTemperature;

                    airBeforeBurnersFlowmeter.Flowrate = airBeforeBurnersFlowrate;
                    gasOnBoilerFlowmeter.Flowrate = gasBeforeBoilerFlowrate;
                    supheatSteamAfterBoilerFlowmeter.Flowrate = supheatSteamAfterBoilerFlowrate;
                    feedwaterWaterOnBoilerFlowmeter.Flowrate = feedwaterOnBoilerFlowrate;
                    waterLevelOnDrum.Flowrate = drumWaterLevel;

                    supHeatSteamAfterBoilerManometer.Update();
                    feedwaterOnBoilerManometer.Update();
                    steamAfterBoilerThermometer.Update();
                    steam2AfterBoilerThermometer.Update();
                    feedwaterAndExhGasesThermometer.Update();
                    airBeforeBurnersFlowmeter.Update();
                    gasOnBoilerFlowmeter.Update();
                    supheatSteamAfterBoilerFlowmeter.Update();
                    feedwaterWaterOnBoilerFlowmeter.Update();
                    waterLevelOnDrum.Update();
                }
            }
        }

        static void window_MouseMoved(object sender, MouseMoveEventArgs e)
        {
            mouseX = e.X;
            mouseY = e.Y;
        }

        static void window_KeyPressed(object sender, SFML.Window.KeyEventArgs e)
        {
            var window = (Window)sender;
            if (e.Code == Keyboard.Key.Escape)
                window.Close();
        }

        private static void OnClose(object sender, EventArgs e)
        {
            RenderWindow window = (RenderWindow)sender;
            window.Close();
        }

        public static Vector2f Position { get; set; }
    }
}
