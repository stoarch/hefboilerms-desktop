﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

/// <summary>
    /// This class is intended to compensate for the lack of an sf::Time equivalent.
    /// It also encapsulates common sf::Time-related functionality.
    /// </summary>
    public class Time
    {
        [DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceCounter(out long lpPerformanceCount);

        [DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceFrequency(out long lpFrequency);

        private readonly long POLL_INTERVAL; // Number of 'ticks' per second
        private readonly double POLL_MULTIPLIER; // Multiply by this to convert ticks to seconds

        private long _monotonic;
        private FpsCounter _fps;

        /// <summary>
        /// Time elapsed since last update, normalised to 1 second.
        /// </summary>
        public double Delta { get; private set; }

        /// <summary>
        /// Total time elapsed since timer was initialised, measured in seconds
        /// </summary>
        public double TotalElapsedTime { get; private set; }

        /// <summary>
        /// Poll for a monotonic time in seconds which doesn't affect other statistics
        /// </summary>
        public double LiveMonotonic {
            get { 
                long ticks;
                QueryPerformanceCounter(out ticks);

                return ticks * POLL_MULTIPLIER;
            }
        }

        /// <summary>
        /// Approximate frames per second based on the last rendered frame
        /// </summary>
        public double Fps { get; private set; }

        /// <summary>
        /// Approximate average frames per second based on the number of frames rendered in the last second
        /// </summary>
        public double FpsAverage { get; private set; }

        public Time()
        {
            QueryPerformanceFrequency(out POLL_INTERVAL);
            POLL_MULTIPLIER = 1d / POLL_INTERVAL;

            _fps = new FpsCounter(POLL_INTERVAL);            

            Restart();
        }

        public void Restart()
        {
            Fps = 0;
            FpsAverage = 0;
            Delta = 0;
            TotalElapsedTime = 0;

            QueryPerformanceCounter(out _monotonic);
        }

        /// <summary>
        /// Call once per frame.
        /// </summary>
        public void Update()
        {
            long ticks;
            QueryPerformanceCounter(out ticks);

            Delta = (ticks - _monotonic) * POLL_MULTIPLIER;
            TotalElapsedTime += Delta;
            Fps = 1 / Delta;
            FpsAverage = _fps.Update(ticks);

            _monotonic = ticks;
        }

        private class FpsCounter
        {
            private long _frames; // Number of frames elapsed since last FPS calculation
            private long _nextTicks; // When to next calculate FPS
            private float _fps; // Last calculated FPS

            private readonly long _timerFrequency;
            private readonly double _timerMultiplier;

            public FpsCounter(long timerFrequency)
            {
                _timerFrequency = timerFrequency;
                _timerMultiplier = 1d / _timerFrequency;
            }

            public float Update(long ticks)
            {
                _frames++;

                if (_nextTicks <= ticks) {
                    _fps = (float)Math.Round(_frames * (ticks - _nextTicks + _timerFrequency) * _timerMultiplier, 2);
                    _frames = 0;
                    _nextTicks = ticks + _timerFrequency;
                }

                return _fps;
            }
        }
}
