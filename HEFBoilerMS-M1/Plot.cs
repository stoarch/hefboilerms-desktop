﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HEFBoilerMS_M1
{
    public class Plot : Transformable, Drawable
    {
        public Plot(Vector2f size, String title)
        {
            grid = new Grid();
            xaxis = new Axis();
            yaxis = new Axis();
            bg = new RectangleShape();
            curves = new Dictionary<string, Curve>();

            this.title = new Text();
            SetTitle(title);
            setSize(size);
        }

        public Curve createCurve(String name, Color color)
        {
            var curve = new Curve(graphSize, color);
            curve.SetLabel(name);
            curve.Position = graphPos;
            curves[name] = curve;
            return curve;
        }

        public bool haveCurve(String name)
        {
            return curves.Any( x => x.Key == name );
        }

        public Curve getCurve(String name)
        {
            if (haveCurve(name) == false)
                throw new Exception("Curve not found:" + name);
            return curves[name];
        }

        public void Prepare()
        {
            Vector2f rangex;
            Vector2f rangey;

            rangex.X = float.MaxValue;
            rangey.X = 0;

            rangex.Y = float.MaxValue;
            rangey.Y = 0;

            foreach (var curve in curves)
            {
                curve.Value.Prepare(ref rangex, ref rangey);
            }

            xaxis.Prepare(rangex);
            yaxis.Prepare(rangey);
        }

        public void SetXLabel(String name)
        {
            xaxis.SetName(name);
        }

        public void SetYLabel(String name)
        {
            yaxis.SetName(name);
        }

        public void setBackgroundColor(Color color)
        {
            bg.FillColor = color;
        }

        public void setTitleColor(Color color)
        {
            title.Color = (color);
        }

        public void setFont(String filename)
        {
            font = new Font(filename);
            title.Font = font;
            xaxis.SetFont(font);
            yaxis.SetFont(font);
            title.Position = new Vector2f(-title.GetLocalBounds().Width / 2 + size.X / 2, 0);
        }

        public void SetTitle(string atitle)
        {
            title.DisplayedString = atitle;
            title.CharacterSize = 16;
        }

        public void setPosition( Vector2f pos )
        {
            Position = pos;

            title.Position = new Vector2f(10, -30 );

            graphPos.X = size.X * (kLeftSize / 100.0f);
            graphPos.Y = size.Y * (kTopSize / 100.0f);

            graphSize.X = size.X - kBorderSize * 2
                -size.X *(kLeftSize/100.0f)
                -size.X *(kRightSize/100.0f);
            graphSize.Y = size.Y - kBorderSize * 2
                - size.Y * (kTopSize / 100.0f)
                - size.Y * (kBottomSize / 100.0f);

            xaxis.Position = new Vector2f(graphPos.X, graphPos.Y + graphSize.Y);
            yaxis.Position = new Vector2f(graphPos.X - 20, graphPos.Y + graphSize.Y);
        }

        public void setSize(Vector2f size)
        {
            this.size = size;
            graphPos.X = size.X * (kLeftSize / 100.0f);
            graphPos.Y = size.Y * (kTopSize / 100.0f);

            graphSize.X = size.X - kBorderSize * 2
                -size.X *(kLeftSize/100.0f)
                -size.X *(kRightSize/100.0f);
            graphSize.Y = size.Y - kBorderSize * 2
                - size.Y * (kTopSize / 100.0f)
                - size.Y * (kBottomSize / 100.0f);

            grid.Setup(graphSize);
            grid.Position = graphPos;

            xaxis.SetSize(graphSize.X);
            yaxis.SetSize(graphSize.Y);

            xaxis.Position = new Vector2f(graphPos.X, graphPos.Y + graphSize.Y);
            yaxis.Position = new Vector2f(graphPos.X - 20, graphPos.Y + graphSize.Y);
            yaxis.Rotation = -90;

            bg.Size = new Vector2f(size.X - kBorderSize * 2, size.Y - kBorderSize * 2);
            bg.FillColor = new Color(220, 220, 200);
            bg.OutlineColor = new Color(200, 0, 0);
            bg.OutlineThickness = kBorderSize;
            bg.Position = new Vector2f(kBorderSize, kBorderSize);
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            states.Transform *= Transform;

            target.Draw(bg, states);
            target.Draw(grid, states);
            target.Draw(title, states);
            target.Draw(xaxis, states);
            target.Draw(yaxis, states);

            foreach (var item in curves)
            {
                target.Draw(item.Value, states);
            }

        }

        private Axis xaxis;
        private Axis yaxis;

        private Grid grid;
        private Vector2f size;

        private Dictionary<String, Curve> curves;
        private Vector2f graphSize;
        private Vector2f graphPos;

        const int kTopSize = 10;
        const int kLeftSize = 12;
        const int kRightSize = 5;
        const int kBottomSize = 12;
        const int kBorderSize = 2;

        private Font font;
        private Text title;
        private RectangleShape bg;
    }
}
