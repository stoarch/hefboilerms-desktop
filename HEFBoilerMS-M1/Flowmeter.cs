﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HEFBoilerMS_M1
{
    public class Flowmeter 
    {
        Texture back;
        Sprite backSprite;

        Texture arrow;
        Sprite arrowSprite;

        RenderWindow window;
        Font font;

        RectangleShape bgRect;

        Text captionText;
        Text flowrateText;

        Text text0;
        Text text100;
        Text text200;
        Text text300;
        Text text400;
        Text text500;
        Text text600;
        Text text700;

        String caption;

        public Flowmeter(RenderWindow awindow, Font afont, String acaption)
        {
            window = awindow;
            font = afont;
            caption = acaption;

            back = new Texture(".\\media\\flowmeter.png");
            backSprite = new Sprite(back);

            arrow = new Texture(".\\media\\flowmeter_arrow.png");
            arrowSprite = new Sprite(arrow);

            backSprite.Position = new Vector2f(100f, 100f);
            arrowSprite.Position = new Vector2f(100f, 200f);

            captionText = new Text(caption, font);
            captionText.CharacterSize = 12;
            captionText.Color = Color.Black;
            captionText.Position = new Vector2f(20, 230);

            flowrateText = new Text("0.00", font);
            flowrateText.CharacterSize = 20;
            flowrateText.Color = Color.Yellow;
            flowrateText.Position = new Vector2f(100, 150);

            bgRect = new RectangleShape(new Vector2f(270.0f, 70.0f));
            bgRect.FillColor = Color.White;
            bgRect.Position = new Vector2f(139, 139);
            bgRect.Origin = new Vector2f(0, 0);

            text0 = makeText("0", new Vector2f(17, 211));
            text100 = makeText("100", new Vector2f(47, 211));
            text200 = makeText("200", new Vector2f(47, 211));
            text300 = makeText("300", new Vector2f(47, 211));
            text400 = makeText("400", new Vector2f(47, 211));
            text500 = makeText("500", new Vector2f(47, 211));
            text600 = makeText("600", new Vector2f(47, 211));
            text700 = makeText("700", new Vector2f(47, 211));
        }

        Text makeText(String caption, Vector2f pos)
        {
            var text = new Text(caption, font);
            text.CharacterSize = 12;
            text.Color = Color.Black;
            text.Position = pos;
            return text;
        }

        public void Draw()
        {
            window.Draw(bgRect);
            window.Draw(backSprite);
            window.Draw(arrowSprite);
            window.Draw(captionText);
            window.Draw(flowrateText);

            window.Draw(text0);
            window.Draw(text100);
            window.Draw(text200);
            window.Draw(text300);
            window.Draw(text400);
            window.Draw(text500);
            window.Draw(text600);
            window.Draw(text700);
        }

        internal void Update()
        {
        }

        private float flowrate;

        const float MIN_FLOWRATE = 0;
        const float MAX_FLOWRATE = 700;
        const float DELTA_FLOWRATE = MAX_FLOWRATE - MIN_FLOWRATE;

        const float RULER_START = 100;
        const float RULER_END = 240;
        const float RULER_HEIGHT = RULER_END - RULER_START;

        public float Flowrate 
        {
            get
            {
                return flowrate;
            }
            set
            {
                flowrate = value;
                UpdateValueText();
                UpdateArrowPosition();
                UpdateFlowratePosition();
            }
        }

        private void UpdateArrowPosition()
        {
            if (flowrate > MAX_FLOWRATE)
            {
                arrowSprite.Position = new Vector2f( position.X + RULER_END - 17, position.Y);
                return;
            }

            float dx = (flowrate - MIN_FLOWRATE) / (DELTA_FLOWRATE) * RULER_HEIGHT;
            arrowSprite.Position = new Vector2f(position.X + 85 + dx, position.Y);
        }

        private void UpdateValueText()
        {
            flowrateText.DisplayedString = flowrate.ToString("###.00"); 
        }

        private Vector2f position;
        public Vector2f Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                UpdatePosition();
            }
        }

        private void UpdatePosition()
        {
            backSprite.Position = position;
            captionText.Position = new Vector2f(position.X + 5, position.Y + 50);

            UpdateFlowratePosition();
            bgRect.Position = position;
            UpdateArrowPosition();
            UpdateTextPosition();
        }

        private void UpdateTextPosition()
        {
            float dx = 0, dy = 20;
            text0.Position = new Vector2f(position.X + dx + RULER_START, position.Y +  dy);
            text100.Position = new Vector2f(position.X + dx + RULER_START + 8, position.Y + dy + 20);
            text200.Position = new Vector2f(position.X + dx + RULER_START + 30, position.Y + dy - 20);
            text300.Position = new Vector2f(position.X + dx + RULER_START + 47, position.Y + dy + 20);
            text400.Position = new Vector2f(position.X + dx + RULER_START + 67, position.Y + dy - 20);
            text500.Position = new Vector2f(position.X + dx + RULER_START + 87, position.Y + dy + 20);
            text600.Position = new Vector2f(position.X + dx + RULER_START + 110, position.Y + dy - 20);
            text700.Position = new Vector2f(position.X + dx + RULER_START + 127, position.Y + dy + 20);
        }

        private void UpdateFlowratePosition()
        {
            FloatRect bounds = flowrateText.GetLocalBounds();
            flowrateText.Position = new Vector2f(position.X + 60 - bounds.Width/2, position.Y + 17);
        }


    }
}
